//include header
#include "filter.h"
#include "showHistogram.h"

int main(int argc, char** argv)
{
	//Load an image 
	cv::Mat image = cv::imread("barbecue.png");

	//Part 1a: Show original histogran
	histogram org;
	org.showOriginal(image);

	//Part 1b: Show equalization
	histogramEqualization equ;
	equ.showEqualized(image);

	//Part 2: Image filtering
	//Median Blur
	median med;
	med.medianBlur(image);

	//Gaussian Blur
	gaussian gau;
	gau.gaussianBlur(image);

	//Bilateral Filter
	bilateral bil;
	bil.bilateralFilter(image);

	cv::waitKey(0);
	cv::destroyAllWindows();

	return 0;
};

