#include <opencv2\opencv.hpp>
#include <opencv2\core.hpp>
#include <iostream>

using namespace cv;
using namespace std;
//Part 1: Histogram Equalization

vector<Mat> hists, equaBGR, imgBGR;
Mat b_hist, g_hist, r_hist, b_dst, g_dst, r_dst;
int histSize = 256;
float hranges[] = { 0, 255 };
Mat bgr_planes[3];
const float * histRange[] = { hranges };

//1a. Show original histogram

class histogram
{
public: void showOriginal(Mat image)
{
	//Split img to B,R,G
	split(image, bgr_planes);

	//Histogram calculation
	cv::calcHist(&bgr_planes[0], 1, 0, Mat(), b_hist, 1, &histSize, histRange, true, false);
	cv::calcHist(&bgr_planes[1], 1, 0, Mat(), g_hist, 1, &histSize, histRange, true, false);
	cv::calcHist(&bgr_planes[2], 1, 0, Mat(), r_hist, 1, &histSize, histRange, true, false);

	//Add each B,G,R histogram to a MAT vector
	hists.push_back(b_hist);
	hists.push_back(g_hist);
	hists.push_back(r_hist);

	//Show original histogram
	showHistogram(hists);
	cv::imshow("Orginal Image", image);
	cv::moveWindow("Orginal Image", 0, 200);

}

private: void showHistogram(std::vector<cv::Mat>& hists)
{
	// Min/Max computation
	double hmax[3] = { 0,0,0 };
	double min;
	cv::minMaxLoc(hists[0], &min, &hmax[0]);
	cv::minMaxLoc(hists[1], &min, &hmax[1]);
	cv::minMaxLoc(hists[2], &min, &hmax[2]);

	cv::Scalar colors[3] = { cv::Scalar(255,0,0), cv::Scalar(0,255,0),
							 cv::Scalar(0,0,255) };

	std::vector<cv::Mat> canvas(hists.size());

	std::string wname[3] = { "blue original", "green original", "red original" };
	for (int i = 0, end = hists.size(); i < end; i++)
	{
		canvas[i] = cv::Mat::ones(125, hists[0].rows, CV_8UC3);

		for (int j = 0, rows = canvas[i].rows; j < hists[0].rows - 1; j++)
		{
			cv::line(
				canvas[i],
				cv::Point(j, rows),
				cv::Point(j, rows - (hists[i].at<float>(j) * rows / hmax[i])),
				hists.size() == 1 ? cv::Scalar(200, 200, 200) : colors[i],
				1, 8, 0
			);
		}

		cv::imshow(hists.size() == 1 ? "value" : wname[i], canvas[i]);
		cv::moveWindow(wname[i], i * 256, 0);

	}
}
};


//1b. Image equalization

class histogramEqualization
{
public: void showEqualized(Mat image)
{
	//Split img to B,R,G
	split(image, bgr_planes);

	//Equalizing each B,G,R images
	cv::equalizeHist(bgr_planes[0], b_dst);
	cv::equalizeHist(bgr_planes[1], g_dst);
	cv::equalizeHist(bgr_planes[2], r_dst);

	//Histogram calculation
	cv::calcHist(&b_dst, 1, 0, Mat(), b_hist, 1, &histSize, histRange, true, false);
	cv::calcHist(&g_dst, 1, 0, Mat(), g_hist, 1, &histSize, histRange, true, false);
	cv::calcHist(&r_dst, 1, 0, Mat(), r_hist, 1, &histSize, histRange, true, false);

	//Add each B,G,R histogram to a MAT vector
	equaBGR.push_back(b_hist);
	equaBGR.push_back(g_hist);
	equaBGR.push_back(r_hist);

	//Show histogram
	showHistogramEqualized(equaBGR);

	// Show equalized image 
	Mat imgEquOut;

	//Add each B,G,R img to a MAT vector
	imgBGR.push_back(b_dst);
	imgBGR.push_back(g_dst);
	imgBGR.push_back(r_dst);

	//Merge to 1 img
	cv::merge(imgBGR, imgEquOut);

	//Show an equlized image
	cv::imshow("Equalized Image", imgEquOut);
	cv::moveWindow("Equalized Image", 1024, 200);

}

private: void showHistogramEqualized(std::vector<cv::Mat>& hists)
{
	// Min/Max computation
	double hmax[3] = { 0,0,0 };
	double min;
	cv::minMaxLoc(hists[0], &min, &hmax[0]);
	cv::minMaxLoc(hists[1], &min, &hmax[1]);
	cv::minMaxLoc(hists[2], &min, &hmax[2]);

	cv::Scalar colors[3] = { cv::Scalar(255,0,0), cv::Scalar(0,255,0),
							 cv::Scalar(0,0,255) };

	std::vector<cv::Mat> canvas(hists.size());

	std::string wname[3] = { "blue equalized", "green equalized", "red equalized" };
	for (int i = 0, end = hists.size(); i < end; i++)
	{
		canvas[i] = cv::Mat::ones(125, hists[0].rows, CV_8UC3);

		for (int j = 0, rows = canvas[i].rows; j < hists[0].rows - 1; j++)
		{
			cv::line(
				canvas[i],
				cv::Point(j, rows),
				cv::Point(j, rows - (hists[i].at<float>(j) * rows / hmax[i])),
				hists.size() == 1 ? cv::Scalar(200, 200, 200) : colors[i],
				1, 8, 0
			);
		}

		cv::imshow(hists.size() == 1 ? "value" : wname[i], canvas[i]);
		cv::moveWindow(wname[i], i * 256 + 1024, 0);

	}
}
};
