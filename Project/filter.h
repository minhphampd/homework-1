#include <opencv2\opencv.hpp>
#include <opencv2\imgproc.hpp>
#include <opencv2\core.hpp>

using namespace cv;

int ksize;
double sigma, sigma2;
Mat medianOut, gaussianOut, bilateralOut;

class median
{
public: void medianBlur(Mat image)
{
	// Create Window
	namedWindow("MedianBlur", WINDOW_AUTOSIZE);

	// Create Trackbar
	cv::createTrackbar("ksize", "MedianBlur", 0, 20);

	while (1) {

		ksize = 2 * cv::getTrackbarPos("ksize", "MedianBlur") + 1;
		cv::medianBlur(image, medianOut, ksize);
		cv::imshow("MedianBlur", medianOut);

		// Cancel when press ESC
		int outKey = cv::waitKey(20);
		if (outKey == 27)
		{
			break;
		}

	}
}
};

class gaussian
{
public:	void gaussianBlur(Mat image)
{
	// Create Window
	namedWindow("GaussianBlur", WINDOW_AUTOSIZE);

	// Create Trackbar
	cv::createTrackbar("ksize", "GaussianBlur", 0, 20);
	cv::createTrackbar("sigma", "GaussianBlur", 0, 300);

	while (1) {

		ksize = 2 * cv::getTrackbarPos("ksize", "GaussianBlur") + 1;
		sigma = cv::getTrackbarPos("sigma", "GaussianBlur");

		cv::GaussianBlur(image, gaussianOut, Size(ksize, ksize), sigma, BORDER_DEFAULT);
		cv::imshow("GaussianBlur", gaussianOut);

		// Cancel when press ESC
		int outKey = cv::waitKey(20);
		if (outKey == 27)
		{
			break;
		}

	}
}
};

class bilateral
{
public:	void bilateralFilter(Mat image)
{
	// Create Window
	namedWindow("BilateralFilter", WINDOW_AUTOSIZE);

	// Create Trackbar
	cv::createTrackbar("ksize", "BilateralFilter", 0, 50);
	cv::createTrackbar("sigma_r", "BilateralFilter", 0, 300);
	cv::createTrackbar("sigma_s", "BilateralFilter", 0, 300);

	while (1) {

		ksize = 2 * cv::getTrackbarPos("ksize", "BilateralFilter") + 1;
		sigma = cv::getTrackbarPos("sigma_r", "BilateralFilter");
		sigma2 = cv::getTrackbarPos("sigma_s", "BilateralFilter");

		cv::bilateralFilter(image, bilateralOut, ksize, sigma, sigma2, BORDER_DEFAULT);
		cv::imshow("BilateralFilter", bilateralOut);

		// Cancel when press ESC
		int outKey = cv::waitKey(20);
		if (outKey == 27)
		{
			break;
		}

	}
}
};
